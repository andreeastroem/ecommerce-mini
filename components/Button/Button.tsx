import { Flex } from "@theme-ui/components";
import ButtonIcon from "./ButtonIcon";
import { variants, TVariants } from "./variants";

export default function Button({
  onClick,
  variant = "default",
  text,
}: {
  onClick: () => void;
  variant?: TVariants;
  text: string;
}) {
  return (
    <Flex
      as={`button`}
      onClick={onClick}
      sx={{
        border: "none",
        padding: "sm",
        width: ["100%", null, "fit-content"],
        borderRadius: 8,
        boxShadow: "2px 2px 4px #00000029",
        justifyContent: "center",
        fontSize: "1.25rem",
        gap: "sm",
        ...variants[variant],
      }}
    >
      <ButtonIcon variant={variant} />
      <Flex
        sx={{
          textTransform: "uppercase",
          fontWeight: "bold",
        }}
      >
        {text}
      </Flex>
    </Flex>
  );
}
