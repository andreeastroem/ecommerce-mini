import { Box } from "@theme-ui/components";
import { FaCartArrowDown, FaFingerprint } from "react-icons/fa";
import { TVariants } from "./variants";

export default function ButtonIcon({
  variant = "default",
}: {
  variant?: TVariants;
}) {
  return (
    <Box>
      {variant === "buy" && <FaCartArrowDown />}
      {variant === "default" && <FaFingerprint />}
    </Box>
  );
}
