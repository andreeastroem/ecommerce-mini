export type TVariants = "default" | "buy";

export const variants = {
  default: {
    backgroundColor: "yellowgreen",
    color: "mintcream",
    "&:hover": {
      backgroundColor: "mintcream",
      color: "yellowgreen",
    },
  },
  buy: {
    backgroundColor: "cornflowerblue",
    color: "gold",
    "&:hover": {
      backgroundColor: "gold",
      color: "cornflowerblue",
    },
  },
};
