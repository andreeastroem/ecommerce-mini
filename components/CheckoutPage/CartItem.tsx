import { Box, Flex, Grid, Text } from "@theme-ui/components";
import { useEffect, useState, ReactNode } from "react";
import { FaPlus, FaMinus } from "react-icons/fa";
import {
  addItemToCart,
  getCartItemById,
  removeItemFromCart,
} from "../../lib/cart";

import { TCurrency } from "../../lib/types";

export default function CartItem({
  qty,
  currency,
  localeCurrency,
  price,
  name,
  productTitle,
  qtyTitle,
  priceTitle,
  id,
}: {
  qty: number;
  currency: TCurrency;
  localeCurrency: TCurrency;
  price: number;
  name: string;
  productTitle: string;
  qtyTitle: string;
  priceTitle: string;
  id: string;
}) {
  const [rate, setRate] = useState<number | null>(null);
  // const [quantity, setQuantity] = useState<number>(qty);
  const cartItem = getCartItemById(id);
  useEffect(() => {
    async function getRate() {
      await fetch(
        `/api/exchange?currency=${cartItem?.currency}&to=${localeCurrency}`
      )
        .then((res) => {
          if (res.status !== 200) {
            console.error(
              "Error fetching rate for currency ",
              cartItem?.currency,
              localeCurrency
            );
          }
          return res.json();
        })
        .then((data) => {
          setRate(data.rate);
        });
    }
    getRate();
  }, [cartItem?.currency, localeCurrency]);

  if (!rate) {
    return <Box as="li">{"Waiting..."}</Box>;
  } else if (rate === -1) {
    return <Box as="li">{"Error getting exchange rates"}</Box>;
  }

  const totPrice = rate * price * qty;

  return (
    <ListItem>
      <ColumnItem title={productTitle} value={name} />
      <ColumnItem
        title={priceTitle}
        value={`${totPrice.toFixed(2)} ${localeCurrency}`}
      />
      <ColumnItem title={qtyTitle} value={qty} />
      <Flex sx={{ fontSize: "1.5rem", gap: "sm" }}>
        <Flex
          onClick={() => {
            addItemToCart({
              name: name,
              price: price,
              currency: currency,
              id: id,
              qty: 1,
            });
            // setQuantity(qty + 1);
          }}
          sx={{
            cursor: "pointer",
            "&:active": {
              color: "green",
            },
          }}
        >
          <FaPlus />
        </Flex>
        <Flex
          sx={{
            cursor: "pointer",
            "&:active": {
              color: "green",
            },
          }}
          onClick={() => {
            removeItemFromCart({
              name: name,
              price: price,
              currency: currency,
              id: id,
              qty: 1,
            });
            // setQuantity(quantity - 1);
          }}
        >
          <FaMinus />
        </Flex>
      </Flex>
    </ListItem>
  );
}

function ColumnItem({
  title,
  value,
}: {
  title: string;
  value: string | number;
}) {
  return (
    <Flex sx={{ flexDirection: "column" }}>
      <Text as={`h3`} sx={{ color: "green" }}>
        {title}
      </Text>
      <Text as={`span`}>{value}</Text>
    </Flex>
  );
}

function ListItem({ children }: { children: ReactNode }) {
  return (
    <Grid
      as={`li`}
      columns={[4]}
      sx={{
        gap: "sm",
        padding: "sm",
        backgroundColor: "yellowgreen",
        borderRadius: 12,
        boxShadow: "default",
        color: "white",
        fontWeight: "semibold",
        alignItems: "center",
      }}
    >
      {children}
    </Grid>
  );
}
