import { Flex, Text, Grid } from "@theme-ui/components";
import { useRouter } from "next/router";
import { ILocaleFlag } from "../../lib/types";
import Image from "next/image";
import { FaShoppingCart } from "react-icons/fa";

export default function Header({ locales }: { locales: Array<ILocaleFlag> }) {
  const router = useRouter();
  return (
    <Flex
      sx={{
        width: "100%",
        backgroundColor: "yellowgreen",
        padding: "md",
        justifyContent: "space-between",
        flexWrap: "wrap",
        gap: "sm",
      }}
    >
      <Text
        as={"h1"}
        sx={{ cursor: "pointer" }}
        onClick={() => router.push("/")}
      >
        ecommerce mini
      </Text>
      <Grid
        sx={{
          gap: "0.5rem",
          justifyContent: "flex-end",
        }}
        columns={[3, null, 4]}
      >
        <Flex
          sx={{
            fontSize: "2rem",
            alignItems: "center",
            cursor: "pointer",
            color: "white",
          }}
          onClick={() => {
            router.push("/checkout");
          }}
        >
          <FaShoppingCart />
        </Flex>
        {locales?.map((l) => {
          return (
            <Flex
              key={l.iso}
              sx={{
                position: "relative",
                alignItems: "center",
                cursor: "pointer",
              }}
              onClick={() => {
                router.push(
                  { pathname: router.pathname, query: router.query },
                  router.asPath,
                  { locale: l.iso }
                );
              }}
            >
              <Image
                src={l.flag.url}
                width={32}
                height={32}
                alt={l.flag.description}
                layout="fixed"
              />
              <Flex
                sx={{
                  backgroundColor:
                    router.locale === l.iso ? "unset" : "#99999990",
                  position: "absolute",
                  left: 0,
                  height: 32,
                  width: 32,
                  borderRadius: "50%",
                }}
              />
            </Flex>
          );
        })}
      </Grid>
    </Flex>
  );
}
