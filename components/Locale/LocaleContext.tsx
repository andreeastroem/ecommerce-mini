import { createContext } from "react";
import { TLocale } from "../../lib/types";

export interface ILocaleContext {
  locale: TLocale;
  setLocale: (locale: TLocale) => void;
}

const LocaleContext = createContext<ILocaleContext>({
  locale: "en-US",
  setLocale: () => {},
});

export default LocaleContext;
