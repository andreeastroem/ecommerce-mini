import { createContext, ReactNode, useState } from "react";
import { TLocale } from "../../lib/types";
import LocaleContext from "./LocaleContext";

export default function LocaleProvider({
  locale,
  children,
}: {
  locale: TLocale;
  children: ReactNode;
}) {
  const [l, setLocale] = useState(locale);

  return (
    <LocaleContext.Provider value={{ locale: l, setLocale: setLocale }}>
      {children}
    </LocaleContext.Provider>
  );
}
