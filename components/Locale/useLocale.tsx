import { useContext } from "react";
import LocaleContext, { ILocaleContext } from "./LocaleContext";

export default function useLocale(): ILocaleContext {
  const value = useContext(LocaleContext);

  return useContext(LocaleContext);
}
