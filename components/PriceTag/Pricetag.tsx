import { Flex, Box } from "@theme-ui/components";
import { TCurrency } from "../../lib/types";

export default function PriceTag({
  price,
  currency,
}: {
  price: number;
  currency: TCurrency;
}) {
  return (
    <Flex
      sx={{
        gap: "0.25rem",
        border: "1px dashed goldenrod",
        borderRadius: 8,
        padding: "0.5rem",
        transform: "rotateZ(15deg)",
      }}
    >
      <Box>{price}</Box>
      <Box>{currency}</Box>
    </Flex>
  );
}
