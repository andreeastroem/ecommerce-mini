import {
  TAllProducts,
  TProduct,
  ILocaleFlag,
  TLocale,
  IHomepage,
  ICheckoutPage,
} from "./types";

async function fetchGraphQL(query: string, preview: boolean) {
  return fetch(
    `https://graphql.contentful.com/content/v1/spaces/${process.env.CONTENTFUL_SPACE_ID}`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${
          preview
            ? process.env.CONTENTFUL_PREVIEW_ACCESS_TOKEN
            : process.env.CONTENTFUL_ACCESS_TOKEN
        }`,
      },
      body: JSON.stringify({ query }),
    }
  ).then((res) => res.json());
}

export async function getAllLocales(): Promise<Array<ILocaleFlag>> {
  const localeCollection = await fetchGraphQL(
    `
    query {
      localesCollection {
        items {
          iso
          flag {
            url
            width
            height
            title
            description
            contentType
            fileName
            size
          }
        }
      }
    }
  `,
    false
  );

  return localeCollection?.data?.localesCollection?.items;
}

export async function getProductBySlug(
  slug: string,
  locale: TLocale
): Promise<{ items: Array<TProduct> }> {
  const product = await fetchGraphQL(
    `query {
      productCollection(where: { slug: "${slug}" }${
      locale ? `, locale: "${locale}"` : ""
    }) {
        items {
          name
          description
          releaseDate
          properties
          slug
          price
          currency
          galleryCollection {
            items {
              url
              width
              height
              title
            }
          }
          sys {
            id
          }
          brand
        }
      }
    }`,
    false
  );
  return product?.data?.productCollection?.items?.[0];
}

export async function getLatestProducts(
  locale: TLocale,
  limit?: number
): Promise<TAllProducts> {
  //TODO: Sort by date
  const products = await fetchGraphQL(
    `
        query {
            productCollection(order: releaseDate_DESC, limit: ${limit} ${
      locale ? `, locale: "${locale}"` : ""
    }) {
                items {
                    name
                    description
                    releaseDate
                    properties
                    slug
                    galleryCollection {
                        items {
                            title
                            url
                            width
                            height
                        }
                    }
                    sys {
                      id
                    }
                    brand
                }
            }
        }
        `,
    false
  );

  return products;
}
export async function getAllProducts(locale: TLocale): Promise<TAllProducts> {
  const query = `
  query {
      productCollection ${locale ? `(locale: "${locale}")` : ""} {
          items {
              name
              description
              releaseDate
              properties
              slug
              galleryCollection {
                  items {
                      title
                      url
                      width
                      height
                  }
              }
              sys {
                id
              }
              brand
          }
      }
  }
  `;
  const products = await fetchGraphQL(query, false);

  return products?.data?.productCollection?.items;
}

export async function getHomepage(locale: TLocale): Promise<IHomepage> {
  const query = `
  query {
    homepage(id: "4qtOuau3HXfLX2udgK8VEY" ${
      locale ? `,locale: "${locale}"` : ""
    }) {
      title
      titleDescription
      productHighlightCollection {
        items {
          name
          description
          slug
          galleryCollection {
            items {
              url
              description
            }
          }
        }
      }
    }
  }
  `;
  const homepage = await fetchGraphQL(query, false);
  return homepage.data.homepage;
}

export async function getCheckoutPage(locale: TLocale): Promise<ICheckoutPage> {
  const query = `
  query {
    checkoutPage(id: "6XQhiZdDRJYBgAZMNjQgyO" ${
      locale ? `,locale: "${locale}"` : ""
    }) {
      productName
      quantity
      price
      buttonText
      confirmationText
    }
  }
  `;
  const checkoutPage = await fetchGraphQL(query, false);
  return checkoutPage.data.checkoutPage;
}
