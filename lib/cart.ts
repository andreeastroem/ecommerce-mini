import { TCurrency } from "./types";

export type TItem = {
  qty: number;
  currency: TCurrency;
  price: number;
  name: string;
  id: string;
};

export function addItemToCart(item: TItem) {
  const storage = typeof window !== "undefined" ? window.localStorage : null;
  if (storage) {
    const cart = storage.getItem("cart");
    if (cart) {
      const currentItems = JSON.parse(cart) as Array<TItem>;

      const idx = currentItems.findIndex((i) => {
        if (i.id === item.id) return true;
      });
      console.log("idx", idx);
      if (idx > -1) {
        currentItems[idx].qty += 1;
      } else {
        currentItems.push(item);
      }
      storage.setItem("cart", JSON.stringify(currentItems));
    } else {
      const data = [item];
      storage.setItem("cart", JSON.stringify(data));
    }
    window.dispatchEvent(new Event("storage"));
  }
}

export function removeItemFromCart(item: TItem) {
  const storage = typeof window !== "undefined" ? window.localStorage : null;
  if (storage) {
    const cart = storage.getItem("cart");
    if (cart) {
      const currentItems = JSON.parse(cart) as Array<TItem>;
      const idx = currentItems.findIndex((i) => {
        if (i.id === item.id) return true;
      });
      if (idx > -1) {
        currentItems[idx].qty -= 1;
        if (currentItems[idx].qty === 0) {
          currentItems.splice(idx, 1);
        }
      }
      storage.setItem("cart", JSON.stringify(currentItems));
      window.dispatchEvent(new Event("storage"));
    }
  }
}

export function getCartItems(): Array<TItem> {
  const storage = typeof window !== "undefined" ? window.localStorage : null;
  if (storage) {
    const cart = storage.getItem("cart");
    if (cart) {
      return JSON.parse(cart) as Array<TItem>;
    } else {
      return [];
    }
  } else {
    return [];
  }
}

export function getCartItemById(id: string): TItem | null {
  const storage = typeof window !== "undefined" ? window.localStorage : null;
  if (storage) {
    const cart = storage.getItem("cart");
    if (cart) {
      const items = JSON.parse(cart) as Array<TItem>;
      const i = items.find((item) => {
        return item.id === id;
      });
      if (i) {
        return i;
      }
    }
  }
  return null;
}
