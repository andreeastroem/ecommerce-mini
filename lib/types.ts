export type TAsset = {
  title: string;
  description: string;
  contentType: string;
  fileName: string;
  size: number;
  url: string;
  width: number;
  height: number;
};
export type TCurrency = "$" | "SEK" | "DKK" | "€" | null;
export type TProduct = {
  name: string;
  description: string;
  releaseDate: string;
  properties: Array<string>;
  slug: string;
  price: number;
  currency: TCurrency;
  galleryCollection: {
    items: Array<TAsset>;
  };
  sys: {
    id: string;
  };
  brand: string;
};
export type TPartialProduct = {
  name: string;
  description: string;
  slug: string;
  sys: {
    id: string;
  };
  galleryCollection: {
    items: Array<TAsset>;
  };
};

export type TAllProducts = Array<TPartialProduct>;

export type TLocale = "en-US" | "sv";
export interface ILocaleFlag {
  iso: TLocale;
  flag: TAsset;
}

export interface IHomepage {
  title: string;
  titleDescription: string;
  productHighlightCollection: {
    items: Array<{
      name: string;
      description: string;
      slug: string;
      galleryCollection: {
        items: Array<{
          url: string;
          description: string;
        }>;
      };
    }>;
  };
}

export interface ICheckoutPage {
  productName: string;
  quantity: string;
  price: string;
  buttonText: string;
  confirmationText: string;
}
