import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ThemeProvider } from "@theme-ui/core";
import LocaleProvider from "../components/Locale/LocaleProvider";

const theme = {
  colors: {
    background: "#eee",
  },
  space: {
    "2xs": "0.25rem",
    xs: "0.5rem",
    sm: "0.75rem",
    md: "1rem",
  },
  breakpoints: ["321px", "376px", "426px", "769px", "1025px", "1441px"],
  shadows: {
    default: "1px 1px 4px #00000029",
  },
};

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <LocaleProvider locale={"en-US"}>
        <Component {...pageProps} />
      </LocaleProvider>
    </ThemeProvider>
  );
}

export default MyApp;
