import type { NextApiRequest, NextApiResponse } from "next";

type TData = {
  rate: number;
};
type TCurrency = "DKK" | "SEK" | "$" | "€" | undefined;

const exchangeTable = {
  DKK: {
    SEK: 1.43,
    DKK: 1,
    $: 0.15,
    "€": 0.13,
  },
  SEK: {
    SEK: 1,
    DKK: 0.7,
    $: 0.11,
    "€": 0.094,
  },
  $: {
    $: 1,
    SEK: 9.36,
    DKK: 6.55,
    "€": 0.88,
  },
  "€": {
    SEK: 10.63,
    DKK: 7.44,
    $: 1.14,
    "€": 1,
  },
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<TData>
) {
  const currency = req.query.currency as TCurrency;
  const to = (req.query.to as TCurrency) ?? "$";
  switch (currency) {
    case "DKK":
      res.send({ rate: exchangeTable[currency]?.[to] });
      break;
    case "SEK":
      res.send({ rate: exchangeTable[currency]?.[to] });
      break;
    case "$":
      res.send({ rate: exchangeTable[currency]?.[to] });
      break;
    case "€":
      res.send({ rate: exchangeTable[currency]?.[to] });
      break;
    default:
      res.statusCode = 400;
      res.send({ rate: -1 });
  }
}
