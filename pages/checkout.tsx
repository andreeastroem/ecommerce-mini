import { Box, Flex, Text } from "@theme-ui/components";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Button from "../components/Button";
import CartItem from "../components/CheckoutPage/CartItem";
import Header from "../components/Header";
import { getAllLocales, getCheckoutPage } from "../lib/api";
import { getCartItems } from "../lib/cart";
import { ILocaleFlag, TLocale, ICheckoutPage, TCurrency } from "../lib/types";

export default function Checkout({
  locales,
  data,
}: {
  locales: Array<ILocaleFlag>;
  data: ICheckoutPage;
}) {
  const [localeCurrency, setLocaleCurrency] = useState<TCurrency>(null);
  const locale = useRouter().locale as TLocale;
  const [cartItems, setCartItems] = useState(getCartItems());

  useEffect(() => {
    setLocaleCurrency(getLocaleCurrency(locale));
  }, [locale]);

  useEffect(() => {
    function handleStorageEvent() {
      console.log("storage event");
      setCartItems(getCartItems());
    }
    window.addEventListener("storage", handleStorageEvent);
    return () => {
      window.removeEventListener("storage", handleStorageEvent);
    };
  }, []);

  return (
    <Box>
      <header>
        <title>mini checkout</title>
        <meta name="description" content={"mini ecommerce checkout page"} />
        <link rel="icon" href="/favicon.ico" />
      </header>
      <Header locales={locales} />
      <Flex
        as={`ul`}
        sx={{
          listStyle: "none",
          padding: "2xs",
          gap: "sm",
          flexDirection: "column",
        }}
      >
        {cartItems.map((item) => {
          return (
            <CartItem
              key={item.id}
              name={item.name}
              qty={item.qty}
              price={item.price}
              currency={item.currency}
              localeCurrency={localeCurrency}
              productTitle={data.productName}
              qtyTitle={data.quantity}
              priceTitle={data.price}
              id={item.id}
            />
          );
        })}
      </Flex>
      <Flex
        sx={{
          gap: "sm",
          padding: "sm",
          backgroundColor: "yellowgreen",
          borderRadius: 12,
          boxShadow: "default",
          color: "white",
          fontWeight: "semibold",
          alignItems: "center",
          marginY: "xs",
        }}
      >
        <Text>Total price</Text>
        <Text>1231</Text>
      </Flex>
      <Flex sx={{ justifyContent: "center" }}>
        <Button
          onClick={() => {
            console.log("Send purchase to backend");
          }}
          variant="default"
          text={data.buttonText}
        />
      </Flex>
    </Box>
  );
}

function getLocaleCurrency(locale: TLocale): TCurrency {
  switch (locale) {
    case "en-US":
      return "$";
    case "sv":
      return "SEK";
    default:
      return null;
  }
}

export async function getStaticProps({
  preview = false,
  locale,
}: {
  preview: boolean;
  locale: TLocale;
}) {
  const locales = await getAllLocales();
  const checkoutPage = await getCheckoutPage(locale);
  return {
    props: {
      preview,
      locales: locales ? locales : [],
      data: checkoutPage,
    },
  };
}
