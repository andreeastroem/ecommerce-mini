import { getAllLocales, getAllProducts, getHomepage } from "../lib/api";
import { IHomepage, ILocaleFlag, TLocale, TProduct } from "../lib/types";
import Image from "next/image";
import { Box, Flex, Text, Grid } from "@theme-ui/components";
import { useRouter } from "next/router";
import Header from "../components/Header";

export default function Home({
  preview = false,
  homepage,
  locales,
  products,
}: {
  preview: boolean;
  homepage: IHomepage;
  locales: Array<ILocaleFlag>;
  products: Array<TProduct>;
}) {
  const router = useRouter();
  return (
    <Box>
      <header>
        <title>Ecommerce mini</title>
        <meta name="description" content={homepage.title} />
        <link rel="icon" href="/favicon.ico" />
      </header>
      <Header locales={locales} />
      <Box sx={{ padding: "md" }}>
        <h2>{homepage.title}</h2>
        <p>{homepage.titleDescription}</p>
        <Flex
          sx={{
            overflowX: "scroll",
            gap: "md",
            scrollSnapType: "x proximity",
          }}
        >
          {homepage.productHighlightCollection.items.map((product) => {
            return (
              <Box
                key={product.name}
                sx={{
                  padding: "md",
                  border: "1px solid #55555520",
                  borderRadius: 10,
                  minWidth: [250, 300, 400],
                  scrollSnapAlign: "center",
                }}
                onClick={() => {
                  router.push(`/products/${product.slug}`);
                }}
              >
                <Text as={"h2"}>{product.name}</Text>
                <Grid
                  columns={[1, null, null, null, null, 2]}
                  sx={{ paddingY: "sm" }}
                >
                  <Text sx={{ fontSize: "0.75rem" }}>
                    {product.description}
                  </Text>
                  <Image
                    src={product.galleryCollection.items[0].url}
                    alt={product.galleryCollection.items[0].description}
                    width={100}
                    height={100}
                    layout="fixed"
                  />
                </Grid>
              </Box>
            );
          })}
        </Flex>
      </Box>
    </Box>
  );
}

export async function getStaticProps({
  preview = false,
  locale,
}: {
  preview: boolean;
  locale: TLocale;
}) {
  const locales = await getAllLocales();
  const homepage = await getHomepage(locale);
  const products = await getAllProducts(locale);
  return {
    props: {
      preview,
      locales: locales ? locales : [],
      homepage: homepage ? homepage : null,
      products: products ? products : [],
    },
  };
}
