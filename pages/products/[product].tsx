import { Box, Flex, Text } from "@theme-ui/components";
import Image from "next/image";

import { getAllLocales, getAllProducts, getProductBySlug } from "../../lib/api";

import { ILocaleFlag, TLocale, TProduct } from "../../lib/types";

import format from "date-fns/format";
import Header from "../../components/Header";
import Button from "../../components/Button";
import { useRouter } from "next/router";
import PriceTag from "../../components/PriceTag";

export default function Product({
  productInfo,
  locales,
}: {
  productInfo: TProduct;
  locales: Array<ILocaleFlag>;
}) {
  const router = useRouter();
  if (productInfo === null) {
    return <Box>No product info found</Box>;
  }

  const buyText = getBuyText(router.locale as TLocale);

  return (
    <Box>
      <header>
        <title>{`Mini ${productInfo.name}`}</title>
        <meta
          name="description"
          content={`A page about ${productInfo.name} `}
        />
        <link rel="icon" href="/favicon.ico" />
      </header>
      <Header locales={locales} />
      <Box sx={{ padding: "md" }}>
        <Flex sx={{ alignItems: "center", justifyContent: "space-between" }}>
          <Box>
            <Text as={"h2"}>{productInfo?.name ?? "namn"}</Text>
            <Text as="span">
              {format(new Date(productInfo.releaseDate), "yyyy-MM-dd")}
            </Text>
          </Box>
          <PriceTag price={productInfo.price} currency={productInfo.currency} />
        </Flex>
        <Text as={"p"} sx={{ paddingY: "sm" }}>
          {productInfo.description}
        </Text>
        <Flex sx={{ paddingY: "sm" }}>
          {productInfo.galleryCollection.items.map((picture) => {
            return (
              <Box
                key={picture.url}
                sx={{
                  width: [100, null, null, 150, 200, 350],
                  height: [100, null, null, 150, 200, 350],
                  position: "relative",
                }}
              >
                <Image
                  src={picture.url}
                  alt={picture.title}
                  layout={"fill"}
                  objectFit={"contain"}
                />
              </Box>
            );
          })}
        </Flex>
        <Box
          as="ul"
          sx={{
            display: "flex",
            gap: "0.5rem",
            padding: 0,
            flexWrap: "wrap",
            listStyle: "none",
          }}
        >
          {productInfo.properties.map((property) => {
            return (
              <Box
                as="li"
                key={property}
                sx={{
                  borderRadius: 4,
                  padding: "0.5rem",
                  minWidth: "fit-content",
                  boxShadow: "1px 1px 2px #22222250",
                  color: "white",
                  backgroundColor: "goldenrod",
                  fontSize: 12,
                  letterSpacing: 1.2,
                }}
              >
                {property.toUpperCase()}
              </Box>
            );
          })}
        </Box>
        <Box sx={{ paddingY: "sm" }}>
          <Button
            onClick={() => {
              const storedCurrentCart = window.localStorage.getItem("cart");
              if (storedCurrentCart) {
                const currentCart = JSON.parse(storedCurrentCart);
                const cart = {
                  ...currentCart,
                  [productInfo.name]: {
                    qty: currentCart[productInfo.name]
                      ? currentCart[productInfo.name].qty + 1
                      : 1,
                    price: productInfo.price,
                    currency: productInfo.currency,
                  },
                };
                window.localStorage.setItem("cart", JSON.stringify(cart));
              } else {
                const cart = {
                  [productInfo.name]: {
                    qty: 1,
                    price: productInfo.price,
                    currency: productInfo.currency,
                  },
                };
                window.localStorage.setItem("cart", JSON.stringify(cart));
              }
            }}
            text={buyText}
            variant={"buy"}
          />
        </Box>
      </Box>
    </Box>
  );
}

function getBuyText(locale: TLocale) {
  switch (locale) {
    case "en-US":
      return "Buy";
    case "sv":
      return "Köp";
  }
}

export async function getStaticPaths() {
  const products = await getAllProducts("en-US");
  const locales = await getAllLocales();
  const paths = products.data.productCollection.items.reduce<
    Array<{ params: { product: string }; locale: TLocale }>
  >((acc, product) => {
    locales.forEach((locale) => {
      acc.push({
        params: {
          product: product.slug,
        },
        locale: locale.iso,
      });
    });
    return acc;
  }, []);

  return { paths, fallback: false };
}

export async function getStaticProps({
  params,
  locale,
}: {
  params: { product: string };
  locale: TLocale;
}) {
  const productInfo = await getProductBySlug(params.product, locale);
  const locales = await getAllLocales();
  return { props: { productInfo, locales } };
}
